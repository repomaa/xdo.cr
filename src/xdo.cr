require "./lib_xdo"
require "./xdo/window"

class Xdo
  VERSION = "0.1.0"

  alias SearchMode = LibXdo::SearchMode

  @xdo : LibXdo::Xdo

  def initialize(display = nil)
    @xdo = LibXdo.new(display)
  end

  def to_unsafe
    @xdo
  end

  def finalize
    LibXdo.free(self)
  end

  def version
    String.new(LibXdo.version)
  end

  def active_window
    LibXdo.get_active_window(self, out window)
    Window.new(self, window)
  end

  def select_window_with_click
    LibXdo.select_window_with_click(self, out window)
    Window.new(self, window)
  end

  def search_windows(**args : **T) forall T
    search = LibXdo::Search.new
    search.max_depth = -1

    {% for key in T.keys %}
      {% key = key.symbolize %}
      {% if %i[max_depth desktop].includes?(key) %}
        search.{{key.id}} = args[{{key}}].to_i64
      {% elsif key == :only_visible %}
        search.{{key.id}} = args[{{key}}] ? 1 : 0
      {% elsif key == :limit %}
        search.{{key.id}} = args[{{key}}].to_u32
      {% else %}
        search.{{key.id}} = args[{{key}}]
      {% end %}
    {% end %}

    {% begin %}
      {% fields = T.keys.select { |key| LibXdo::SearchMask.constants.includes?(key.camelcase) } %}
      search.mask = {{fields.map { |field| "LibXdo::SearchMask::#{field.camelcase}" }.join(" | ").id}}
    {% end %}

    LibXdo.search_windows(self, pointerof(search), out windows, out num_results)

    Array.new(num_results) do |i|
      Window.new(self, windows[i])
    end
  end
end
