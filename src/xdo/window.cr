require "../lib_xdo"

class Xdo
  class Window
    def initialize(@xdo : Xdo, @window : LibXdo::Window)
    end

    def to_unsafe
      @window
    end

    def name
      LibXdo.get_window_name(@xdo, self, out name, out name_length, out name_type)
      name = Slice(UInt8).new(name, name_length)
      String.new(name)
    end

    def enter_text(text : String, delay = 12000_u64)
      LibXdo.enter_text_window(@xdo, self, text, delay.to_u64)
    end

    def send_keysequence(keysequence : String, delay = 12000_u64)
      LibXdo.send_keysequence_window(@xdo, self, keysequence, delay.to_u64)
    end

    def activate!
      LibXdo.activate_window(@xdo, self)
    end
  end
end
