@[Link(ldflags: "-lxdo")]
lib LibXdo
  type Xdo = Void*
  type Window = Void*

  enum SearchMode : Int32
    Any
    All
  end

  enum SearchMask : UInt32
    Class       = 1_u32 << 1
    Name        = 1_u32 << 2
    Pid         = 1_u32 << 3
    OnlyVisible = 1_u32 << 4
    Screen      = 1_u32 << 5
    ClassName   = 1_u32 << 6
    Desktop     = 1_u32 << 7
  end

  struct Search
    _title : UInt8* # deprecated
    class : UInt8*
    class_name : UInt8*
    name : UInt8*
    pid : Int32
    max_depth : Int64
    only_visible : Int32
    screen : Int32
    mode : SearchMode
    mask : SearchMask
    desktop : Int64
    limit : UInt32
  end

  fun version = xdo_version : UInt8*
  fun new = xdo_new(display : UInt8*) : Xdo
  fun free = xdo_free(xdo : Xdo)
  fun enter_text_window = xdo_enter_text_window(xdo : Xdo, window : Window, string : UInt8*, delay : UInt64)
  fun send_keysequence_window = xdo_send_keysequence_window(xdo : Xdo, window : Window, keysequence : UInt8*, delay : UInt64)
  fun activate_window = xdo_activate_window(xdo : Xdo, window : Window)
  fun get_active_window = xdo_get_active_window(xdo : Xdo, window : Window*)
  fun select_window_with_click = xdo_select_window_with_click(xdo : Xdo, window : Window*)
  fun search_windows = xdo_search_windows(xdo : Xdo, search : Search*, windows : Window**, num_results : UInt32*)
  fun get_window_name = xdo_get_window_name(xdo : Xdo, window : Window, name : UInt8**, name_length : Int32*, name_type : Int32*)
end
